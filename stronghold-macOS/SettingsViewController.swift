//
//  ViewController.swift
//  stronghold-macOS
//
//  Created by Aaron Lichtman on 7/1/18.
//  Copyright © 2018 Aaron Lichtman. All rights reserved.
//

import Cocoa

class ViewController: NSViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override var representedObject: Any? {
        didSet {
        // Update the view, if already loaded.
        }
    }
}

